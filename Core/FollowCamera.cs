﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        //CAMERA ROTATION
        
        /* [Range(0,1)][SerializeField] float rotationSpeed = 0.1f;
        float mouseClickPosX;
        void Update()
        {
            if (Input.GetMouseButtonDown(2))
            {
                mouseClickPosX = Input.mousePosition.x;
            }
            
            if (Input.GetMouseButton(2))
            {
                float yRotation = (Input.mousePosition.x - mouseClickPosX) 
                    * rotationSpeed * Time.deltaTime;
                transform.Rotate(0, yRotation, 0);
            }
        } */
        [SerializeField] Transform target;
        void LateUpdate() 
        {   
            transform.position = target.position;
        }
    }
}
